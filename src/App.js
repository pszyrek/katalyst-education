import React, {Component} from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      app_ID: "client_id=de90a1b4b8aacc03f8143e352bcf7f93f247bac07692a6f4863d4a40c9c12342",
      link: "https://api.unsplash.com/",
      username: "anniespratt",
      pictures: [],
      per_page: 10
    };
    this.generateData = this
      .generateData
      .bind(this);
    this.handleScroll = this
      .handleScroll
      .bind(this);
  }

  componentDidMount() {
    this.generateData();
    window.addEventListener('scroll', this.handleScroll);
  }

  generateData() {
    fetch(this.state.link + "/photos/random?count=" + this.state.per_page + "&" + this.state.app_ID).then((res) => {
      return res.json()
    }).then(data => {
      let pictures = data.map((pic) => {
        return (
          <div key={pic.id}>
            <img src={pic.urls.thumb} alt={pic.description}/>
          </div>
        )
      })
      console.log(pictures)
      this.setState((state) => ({
        pictures: state
          .pictures
          .concat([pictures])
      }))
    })
  }

  handleScroll() {
    const windowHeight = "innerHeight" in window
      ? window.innerHeight
      : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;

    const height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;

    if (windowBottom >= height) {
      this.generateData()
    } else {
      console.log('not a bottom')
    }

  }

  render() {
    return (
      <div className="App">
        {this.state.pictures}
      </div>
    );
  }
}

export default App;